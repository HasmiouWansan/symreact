export const API_URL= 'http://localhost:8000/api/';
export const INVOICE_URL = API_URL+'invoices';
export const CUSTOMER_URL = API_URL+'customers';
export const USERS_URL = API_URL+'users';
export const LOGIN_URL = API_URL+'login_check';

